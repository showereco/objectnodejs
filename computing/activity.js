const EventEmitter = require('events');

class AcitivtyManager extends EventEmitter {
    constructor(thermo) {
        super();
        this.activity = undefined;
        this.timeout = undefined;
        this.thermo = thermo;
    }

    hasActivity() {
        return this.activity !== undefined;
    }

    createActivity(data) {
        if(data.value <= 0) return;
        this.activity = {
            flow: data.value,
            timestamp: data.timestamp,
            start:  data.timestamp,
            consumption: 0,
            temperatures: [this.thermo.read()]     
        }
        this.emit('activity-new', this.activity);
    }

    calculConsumption(old_flow, new_flow, old_timestamp, new_timestamp) {
        let highflow = Math.max(old_flow, new_flow);
        let timepassed = (new_timestamp - old_timestamp)/60000.0;
        let liters = highflow*timepassed;
        return liters;
    }

    updateActivity(data){
        let old_flow = this.activity.flow;
        let old_time = this.activity.timestamp;

        this.activity.consumption+= this.calculConsumption(old_flow, data.value, old_time, data.timestamp);

        this.activity.flow = data.value;
        this.activity.timestamp = data.timestamp;
        this.activity.temperatures.push(this.thermo.read());

        this.emit('activity-update', this.activity);
    }

    finishActivity(){
        this.emit('activity-end', this.activity);
        this.activity = undefined;
        this.timeout = undefined;
    }

    createSessionOrUpdateActivity(data) {
        this.timeout ? this.createSession(data) : this.updateActivity(data);
    }

    createSession(data) {
        clearTimeout(this.timeout);
        this.timeout = undefined;
        this.activity.flow = data.value;
        this.activity.timestamp = data.timestamp;
        this.emit('session-new', this.activity);
    }

    stopSession(data) {
        this.updateActivity(data);
        this.timeout = setTimeout(() => this.finishActivity(), 10000);
        this.emit('session-end', this.activity);
    }

    checkFlow(data) {
        data.value > 0 ? this.createSessionOrUpdateActivity(data) : this.stopSession(data);
    }

    async update(value, timestamp) {
        let data = {value: value, timestamp: timestamp}
        this.activity ? this.checkFlow(data) : this.createActivity(data);
    }

    
}

module.exports = AcitivtyManager;