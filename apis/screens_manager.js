const i2cBus = require('i2c-bus').openSync(1);
const Oled = require('oled-i2c-bus');
const { GroveLCDRGB } = require('grove-lcd-rgb');
const moment = require('moment');
const cron = require('node-cron');
const font = require('oled-font-5x7');
const httpErrors = require('http-errors');

 
const opts = {
  width: 128,
  height: 64,
  address: 0x3c
};
 
const oled = new Oled(i2cBus, opts);

const lcd = new GroveLCDRGB();
lcd.setRGB(0, 0, 0);

oled.clearDisplay();

let popups = [];
let activity = undefined;
let infos = [];
let short_info;
let iter;

let interval;

let mode = 'ACTIVITY';


function clearAll() {
    oled.clearDisplay();
    lcd.clear();
    lcd.setRGB(0, 0, 0);
}

function printClock() {
    let date = moment().format('hh:mm');
    oled.setCursor(10, 20);
    oled.writeString(font, 4, date, 1, true);
}

function clearPopup() {
    let popup = popups.pop();
    if(!popup) return;

    // DO something with popup

    if (popups.length > 0 ) 
    {
        clearAll();
        printPopup(popups[popups.length - 1]) ;
    } else 
    {
        clearAll();
        lcd.setRGB(255, 255, 255);
    }
}

function managePopups() {
    
}

function printPopup(popup) {
    if(popup.left) {
        let screen = popup.left;
        (screen.rgb && screen.rgb.r && screen.rgb.g && screen.rgb.b) ?
        lcd.setRGB(screen.rgb.r, screen.rgb.g, screen.rgb.b) : 
        lcd.setRGB(0, 0, 255);

        lcd.setCursor(0, 0);
        lcd.setText(screen.text);
    }

    if(popup.right) {
        let screen = popup.right;
        oled.clearDisplay();

        (screen.cursor && screen.cursor.x && screen.cursor.y) ?
        oled.setCursor(screen.cursor.x, screen.cursor.y) : 
        oled.setCursor(5, 5);

        oled.writeString(font, screen.size ? screen.size : 2, screen.text, 1, true);
    }
}

function nextInfo() {
    mode = 'ACTIVITY';
    if(!infos.length) return;
    let next = iter.next();
    if (next.done)
    {
        iter = infos[Symbol.iterator]();
        next = iter.next();
    }

    const info = next.value;

    oled.setCursor(info.x, info.y);
    oled.writeString(font, info.size, info.text, 1, true);
}

function updateActivity() {
    mode = 'INFO';
    oled.setCursor(30, 5);
    oled.writeString(font, 2, `${activity.consumption.toFixed(2)} l`, 1, true);
    oled.setCursor(30, 50);
        
    let temperature = activity.temperatures[activity.temperatures.length - 1];
    oled.writeString(font, 2, `${temperature}°`, 1, true);

}

function manageDisplay() {
    if(!iter) iter = infos[Symbol.iterator]();

    if(!short_info)
    {
        lcd.setCursor(9, 0);
        lcd.setTextRaw(`${moment().format('hh:mm')}`);

        let time = moment().valueOf() - activity.start;
        time = ((time/1000)/60).toFixed(0);
        lcd.setCursor(9, 1);
        lcd.setTextRaw(`${time} min`);

        lcd.setCursor(0, 0);
        lcd.setTextRaw(moment().format("MMM Do"));
        lcd.setCursor(0, 1);
        lcd.setTextRaw(`${activity.flow.toFixed(2)} l/m`);
    }

    if(!infos.length) return updateActivity();


    oled.clearDisplay();
    mode === 'ACTIVITY' ? updateActivity() : nextInfo();
}



function register(fastify, options, done) {

    const activityManager = options.activityManager;
    const buttonLock = options.buttonLock;


    activityManager.on('activity-new', (act)=>{ 
        oled.clearDisplay();
        lcd.setRGB(255, 255, 255);
        activity = act;
        
    });
    
    activityManager.on('activity-end', (act)=>{
        activity = undefined;
        popups = [];
        infos = [];
        clearInterval(interval);
        interval = undefined;
        clearAll();
        buttonLock.unlockAll();
    });
    
    activityManager.on('activity-update', (act)=>{
        activity = act;
    });

    fastify.addHook('onRequest', (request, reply, done) => {
        if(!activityManager.hasActivity()) reply.send(httpErrors(403, 'User not in shower'));
        done();
    });
      
    fastify.post('/popup',  (request, reply) => {
        if(!request.body) return reply.send(httpErrors('404', 'No json object'));
        if(!request.body.left && !request.body.right) return reply.send(httpErrors('404', 'No indications found'));
        popups.push(request.body);
        printPopup(request.body);
        buttonLock.addLocker(()=>{
            clearPopup();
        });
        return reply.send('success');
    });

    fastify.post('/info',  (request, reply) => {
        if(!request.body) return reply.send(httpErrors('404', 'No json object'));
        if(!request.body.x || !request.body.y || !request.body.size || !request.body.text) 
            return reply.send(httpErrors('404', 'No indications found'));
        infos.push(request.body);
        return reply.send('success');
    });

    fastify.post('/shortinfo',  (request, reply) => {
        if(!request.body) return reply.send(httpErrors('404', 'No json object'));
        if(!request.body.r || !request.body.g || !request.body.b || !request.body.text) 
            return reply.send(httpErrors('404', 'No indications found'));
        
        short_info = true;
        lcd.clear();
        lcd.setRGB(request.body.r, request.body.g, request.body.b);
        lcd.setCursor(0, 0);
        lcd.setTextRaw(request.body.text);
        setTimeout(() => {
            lcd.clear();
            lcd.setRGB(255,255,255);
            short_info = false;
        }, 3000);

        return reply.send('success');
    });

    setInterval(() => {
        if(!activity) return printClock();
        if(popups.length) return managePopups();

        manageDisplay();
    }, 3000);
   
    done();
}

module.exports = register;