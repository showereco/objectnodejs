const Board = require('node-grovepi').GrovePi.board;
const FlowMeter = require('./sensors/flow-meter');
const Thermo = require('./sensors/thermo');
const Button = require('./sensors/button');
const ActivityManager = require('./computing/activity');
const bonjour = require('bonjour')();
const mqtt = require('mqtt');
const moment = require('moment');
const fastify = require('fastify')();
const httpErrors = require('http-errors');
const readline = require('readline');
const registerScreensAPi  = require('./apis/screens_manager');




function initShower(res) {
    if(!res) return;

    const client  = mqtt.connect('mqtt://localhost');

    const thermo = new Thermo(4);

    const next = new Button(2);
    const prev = new Button(3);
    const play = new Button(8);
    
    const activityManager = new ActivityManager(thermo);
    fastify.register(registerScreensAPi, {prefix: '/screens', activityManager: activityManager, buttonLock: play});

    bonjour.publish({ name: 'ShowerEco', type: 'http', port: 3000 });

    activityManager.on('activity-new', (activity)=>{ 
       client.publish('activity/new', JSON.stringify(activity));
    });
    
    activityManager.on('activity-end', (activity)=>{
        client.publish('activity/end', JSON.stringify(activity));
    });
    
    activityManager.on('activity-update', (activity)=>{
        client.publish('activity/update', JSON.stringify(activity));
    });

    activityManager.on('session-new', (activity)=>{
        client.publish('activity/session/new', JSON.stringify(activity));
    });

    activityManager.on('session-end', (activity)=>{
        client.publish('activity/session/end', JSON.stringify(activity));
    });

    play.on('press', (type) => {
        client.publish('sensors/button/play/press');
    });

    next.on('press', (type) => {
        client.publish('sensors/button/next/press');
    });

    prev.on('press', (type) => {
        client.publish('sensors/button/prev/press');
    });

    client.subscribe('simulation/flowmeter');

    client.on('message', (topic, message) => {
        activityManager.update(parseFloat(message), moment().valueOf());
    });

    fastify.get('/', async(request, reply) => {
        reply.send('OK');
    });

    fastify.get('/activity', async(request, reply) => {
        reply.send(activityManager.activity);
    });

    fastify.get('/temperature', async(request, reply) => {
        reply.send(thermo.read());
    });

    fastify.get('/update/:value', async(request, reply) => {
        activityManager.update(parseFloat(request.params.value), moment().valueOf());
        reply.send('ok');
    });

    fastify.listen(3000, '0.0.0.0', (err, address) => {
        if (err) throw err
        fastify.log.info(`server listening on ${address}`)
    });

}

function onError(err) {
    console.log('Something wrong just happened')
    console.log(err);
}


const board = new Board({debug: true, onInit: initShower, onError: onError});
board.init();
