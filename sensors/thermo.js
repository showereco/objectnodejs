const ds18b20 = require('ds18b20-raspi');

class Thermo {

    
    constructor(gpio = 4, intervalTime = 5000) {
        this.sensor = ds18b20;
        this.intervalTime = intervalTime;
        this.interval = setInterval(this.onInterval.bind(this), this.intervalTime);
        this.temperature = this.onInterval();
    }

    onInterval() {
        try 
        {
            return this.sensor.readSimpleC();
        } catch (err) 
        {
            return NaN;
        }
    }

    read() {
        return this.temperature;
    }
}

module.exports = Thermo;