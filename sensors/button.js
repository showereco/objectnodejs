const EventEmitter = require('events');
const GrovePi = require('node-grovepi').GrovePi;
const ButtonGrove = GrovePi.sensors.DigitalButton;

const DOWN = 0;
const UP = 1;

class Button extends EventEmitter {
    constructor(pin=2) {
        super();
        this.sensor = new ButtonGrove(pin);
        this.sensor.on('change', this.onChange.bind(this));
        this.sensor.watch();
        this.lockers = [];
        this.state = DOWN;
    }

    addLocker(callback) {
        this.lockers.push(callback);
    }

    
    onChange(res){
        if(res === 1 && this.state === DOWN)
        {
            this.date = new Date();
            this.state = UP;
        }

        if(res === 0 && this.state === UP)
        {
            let end = new Date();
            (end.getTime() - this.date.getTime() < 2000) ? 
            this.onPress('singlepress') : this.onPress('longpress');
            this.state = DOWN;
        }
    }

    onPress(type) {
        console.log(type);
        let locker = this.lockers.pop();
        locker ? locker() : this.emit('press', type);
    }

    unlockAll(){
        this.lockers = [];
    }
}

module.exports = Button;

