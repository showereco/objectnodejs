const EventEmitter = require('events');
const Gpio = require('onoff').Gpio;

class FlowMeter extends EventEmitter {

    
    constructor(gpio = 17, intervalTime = 1000) {
        super();
        this.sensor = new Gpio(gpio, 'in', 'falling', {debounceTimeout: 10});
        this.rate = 0;
        this.intervalTime = intervalTime; 
        this.initWatch();
        this.interval = setInterval(this.onInterval.bind(this), this.intervalTime);
    }

    initWatch() {
        this.sensor.watch((err, data) => {
            if(err) throw err;
            ++this.rate;
        });
    }

    onInterval(){
        const save = this.rate;
        this.rate = 0;
        const flow = (save/(this.intervalTime/1000)/7.5);
        this.emit('flow', flow);
    }
}

module.exports = FlowMeter;